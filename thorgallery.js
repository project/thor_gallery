if (Drupal.jsEnabled) {
  Drupal.behaviors.thorgallery = function(context) {
    var options = {
      loader_image: Drupal.settings.thorgallery.loader_image,
      width: Drupal.settings.thorgallery.width,
      height: Drupal.settings.thorgallery.height,
      thumb_opacity: 0.7,
      start_at_index: 0,

      // FIXME: window hash update breaks site navigation if set to true
      update_window_hash: false,

      animate_first_image: false,
      animation_speed: 400,
      display_next_and_prev: true,
      display_back_and_forward: true,
      scroll_jump: 0,
      slideshow: {
        enable: Drupal.settings.thorgallery.autostart,
        autostart: Drupal.settings.thorgallery.autostart,
        speed: Drupal.settings.thorgallery.delay,
        start_label: Drupal.settings.thorgallery.labels.start,
        stop_label: Drupal.settings.thorgallery.labels.stop,
        stop_on_scroll: true,
        countdown_prefix: '(',
        countdown_sufix: ')'
      },
      effect: 'fade',
      enable_keyboard_move: true,
      cycle: true,
      callbacks: {
        init: function() {
          this.preloadImage(0);
          this.preloadImage(1);
          this.preloadImage(2);
        }
      }
    };

    $('.ad-gallery').adGallery(options);
  }
}
