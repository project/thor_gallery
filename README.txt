
INTRODUCTION
------------

Thor Gallery is a robust gallery wrapper for a Facebook album.

Thor Gallery will make it possible to add any public album on Facebook to your
Drupal site. It displays the images in the Facebook album using the AD Gallery
frontend.

REQUIREMENTS
------------

- Facebook account
- Public Facebook album
- Facebook application with App ID and App Secret (see configuration for help)

DEPENDENCIES
------------

Thor gallery module depends on the following libraries:

- Facebook PHP SDK (https://github.com/facebook/facebook-php-sdk)
- AD Gallery (http://adgallery.codeplex.com)

INSTALLATION
------------

1. Install the module as usual, see http://drupal.org/node/70151 for Drupal
   module installation instructions.
2. If Facebook PHP SDK isn't installed already, download and unpack it to
   libraries folder sites/all/libraries/facebook-php-sdk or
   sites/sitename/libraries in multisite environment.
3. If AD Gallery isn't installed already, download AD Gallery and unpack it to
   libraries folder sites/all/libraries/ad-gallery or sites/sitename/libraries
   in multisite environment.

CONFIGURATION
-------------

1. Set up Facebook application:

   1.1 Go to developers.facebook.com
   1.2 Log in
   1.3 Click on Apps in the menu
   1.4 Click on Create New App button to create the application
   1.5 Fill in the App name of your choice (everything else is irrelevant) and
       continue
   1.6 You're done. Save App ID and App Secret, you'll need them to configure
       Thor Gallery

2. Configure Thor Gallery at Administer > Site configuration > Thor Gallery

   2.1 Set Facebook App ID and App Secret
   2.2 Set the width and the height of the gallery
   2.3 Set the delay of the gallery. This will be used to display an image until
       the next one gets animated in
   2.4 Set the autostart value of the gallery. If Autostart is checked,
       slideshow will begin instantly after loading the module.
   2.5 Ensure that the library dependencies are met.
       If all necessary libraries are installed, no errors are displayed on
       module configuration page. Otherwise follow Installation steps 2-3.

CREATING A GALLERY
------------------

Every Thor Gallery item has a Facebook album URL which will be used to fetch the
images. To find the URL of your public Facebook album, navigate to the album
and copy the URL from the location bar. The end of the URL should look like
…facebook.com/media/set/?set=a.272789389397738.78506.197926163550728&type=1

TROUBLESHOOTING
---------------

- Error "No images in this gallery. Sorry!" displayed despite of the images in a
  Facebook album.
  - Confirm if your App ID and Secret are correct. Check the log at
    Administer > Logging and alerts > Database Logging for clues.
  - Make sure your Facebook album is public (see Requirements).
- Error "Facebook PHP SDK not found!" displayed. Check library dependencies, see
  log for a more precise error.
- Error "AD Gallery library not found!" displayed. Check library dependencies,
  see log for a more precise error.

WORKING EXAMPLES
----------------

- http://www.thorsteel.eu/gallery/product-showcase based on
  http://on.fb.me/WbSMoj
- http://www.thorsteel.com.au/gallery/product-showcase based on
  http://on.fb.me/Wen7TZ
