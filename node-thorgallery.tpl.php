<?php
/**
 * @file
 * Node template for the Content Type
 *
 * NB! All $image getters retrieve sanitized and secure data!
 * @see \ThorGalleryImage
 *
 * @ingroup thorgallery
 */
?>
<div><?php echo $body; ?></div>
<?php if (!empty($images)): ?>
  <div class="ad-gallery" style="width:<?php echo $width; ?>px; height:<?php echo $height; ?>px;">
    <div class="ad-image-wrapper" style="width:<?php echo $width; ?>px; height:<?php echo $height; ?>px;"></div>
    <div class="ad-controls"></div>
    <div class="ad-nav">
      <div class="ad-thumbs">
        <ul class="ad-thumb-list">
          <?php foreach ($images as $image): ?>
            <li>
              <a href="<?php echo $image->getImage(); ?>">
                <img src="<?php echo $image->getThumbnail(); ?>" title="<?php echo $image->getCaption(); ?>" />
              </a>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>
  </div>
<?php else: ?>
  <p><?php echo t('No images in this gallery. Sorry!'); ?></p>
<?php endif; ?>
