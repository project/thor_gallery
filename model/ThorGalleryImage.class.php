<?php

/**
 * @file
 * Contains ThorGalleryImage.
 *
 * @ingroup thorgallery
 */

/**
 * Represents image object in the gallery
 */
class ThorGalleryImage {

  /**
   * Absolute full-size image path.
   * @var string
   */
  protected $image;

  /**
   * Absolute thumbnail image path.
   * @var string
   */
  protected $thumbnail;

  /**
   * Caption of the image.
   * @var string
   */
  protected $caption;

  /**
   * Constructs a ThorGalleryImage object.
   *
   * @param array $image
   *   Associative array as returned from Facebook API request
   */
  public function __construct($image) {
    $this->parse($image);
  }

  /**
   * Returns full-size image (absolute) path.
   *
   * @return string
   *   Absolute path to the image at maximum available size
   */
  public function getImage() {
    return $this->image;
  }

  /**
   * Returns thumbnail image (absolute) path.
   *
   * @return string
   *   Absolute path to thumbnail
   */
  public function getThumbnail() {
    return $this->thumbnail;
  }

  /**
   * Returns image caption.
   *
   * @return string
   *   Description text of the image
   */
  public function getCaption() {
    return $this->caption;
  }

  /**
   * Parses associative image array.
   * Sanitizes all data coming from external sources on security purposes.
   */
  protected function parse($image) {
    $this->image = !empty($image['src_big']) ? check_url($image['src_big']) : '';
    $this->thumbnail = !empty($image['src_small']) ? check_url($image['src_small']) : '';
    $this->caption = !empty($image['caption']) ? check_plain($image['caption']) : '';
  }
}
