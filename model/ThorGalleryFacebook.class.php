<?php

/**
 * @file
 * Contains ThorGalleryFacebook.
 *
 * @ingroup thorgallery
 */

/**
 * Extends Facebook class for Facebook API implementation
 */
class ThorGalleryFacebook extends Facebook {

  /**
   * Regex pattern for Facebook album URL validation.
   * @see getAlbumIdFromUrl()
   */
  const ALBUM_URL_PATTERN = '/^(?:http:\/\/)?(?:www\.)facebook\.com\/media\/set\/\?set=a\.\d+\.(\d+)\.(\d+).+$/';

  /**
   * String format pattern for Facebook Album ID formatting.
   * @see getAlbumIdFromUrl()
   */
  const ALBUM_ID_FORMAT = '%s_%s';

  /**
   * Get all images in the Facebook album.
   *
   * @param string $aid
   *   Facebook album ID
   *
   * @return array
   *   Returns array of \ThorGalleryImage
   */
  public function getImages($aid) {
    $images = array();
    try {
      $fql = "SELECT src_big, src_small, caption FROM photo WHERE aid = '$aid'";
      $raw_images = $this->api(array(
          'access_token' => $this->getAccessToken(),
          'method' => 'fql.query',
          'query' => $fql,
        )
      );
      foreach ($raw_images as $image) {
        $images[] = new ThorGalleryImage($image);
      }
    }
    catch (FacebookApiException $exception) {
      watchdog('thorgallery', 'Failed to fetch images: %s', array('%s' => $exception->getMessage()), WATCHDOG_ERROR);
      thorgallery_set_message(t('Failed to fetch images!'));
    }

    return $images;
  }

  /**
   * Get Facebook album ID from absolute URL of the Facebook album.
   *
   * @param string $url
   *   Absolute URL to the Facebook album
   *
   * @return string|null
   *   Returns album ID or null if not found
   */
  public function getAlbumIdFromUrl($url) {
    $matches = array();
    if (preg_match(self::ALBUM_URL_PATTERN, $url, $matches) && isset($matches[1], $matches[2])) {
      $aid = sprintf(self::ALBUM_ID_FORMAT, $matches[2], $matches[1]);
      try {
        $fql = "SELECT aid FROM album WHERE aid = '$aid'";
        $aid_match = $this->api(array(
            'access_token' => $this->getAccessToken(),
            'method' => 'fql.query',
            'query' => $fql,
          )
        );

        if (isset($aid_match)) {
          return $aid;
        }
        else {
          watchdog('thorgallery', 'Invalid album URL: %s', array('%s' => $exception->getMessage()), WATCHDOG_ERROR);
          thorgallery_set_message(t('Invalid album URL!'));
          return NULL;
        }
      }
      catch (Exception $exception) {
        watchdog('thorgallery', 'Failed to fetch album ID: %s', array('%s' => $exception->getMessage()), WATCHDOG_ERROR);
        thorgallery_set_message(t('Failed to fetch album ID!'));
        return NULL;
      }
    }
    watchdog('thorgallery', 'Invalid album URL: %s', array('%s' => $url), WATCHDOG_ERROR);
    thorgallery_set_message(t('Invalid album URL!'));
    return NULL;
  }
}
